﻿    #region LICENSE
/* The MIT License (MIT)
 * 
 * Copyright (c) 2015 Daniel Meszaros <easimer@easimer.tk>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace BuvosNegyzet
{
	public static class LibMagicSquare
	{
        static Random r = new Random();
        /// <summary>
        /// N méretű bűvös négyzetet generál
        /// </summary>
        /// <param name="size">N</param>
        /// <param name="v">Megálljon-e lépésenként</param>
        /// <param name="recursive">ne használd</param>
        /// <returns>N méretű bűvösnégyzet</returns>
		public static int[][] GenerateSquareNovus(int size, bool v, int recursive = 0)
		{
			if (size != 3)
			{
                if (size % 2 == 0 || size < 0 || size == 0)
                {
                    throw new Exception("Csak páratlan mérettel működik az algoritmus");
                }
                int[][] square = new int[size][];
                for (int i = 0; i < square.Length; i++)
                {
                    square[i] = new int[size];
                }
                int max = size * size; //egy N méretű bűvösnégyzetben a legnagyobb szám sose megy N^2 alá
                //Console.WriteLine("Sum: {0} Max: {1}", sum, max);
                int x = Convert.ToInt16(Math.Ceiling(((float)size) / 2)) - 1;
                int y = 0;
                int n = 1;
                square[y][x] = n++; //a legfelső sor középső elemének értéke legyen 1
                //square[y].Count(p => p < 5 && p < 12 && p % 2 == 0);
                while (n <= max)
                {
                    y = (y == 0) ? size - 1 : y - 1; //y legyen N-1 ha y=0, amúgy meg y-1
                    x = (x == size - 1) ? 0 : x + 1; //x legyen 0 ha x=N-1, amúgy meg x+1
                    if (square[y][x] != 0)
                    {
                        //visszalépünk az előző cellába
                        y = (y == size - 1) ? 0 : y + 1; //az előző fordítottja
                        x = (x == 0) ? size - 1 : x - 1; //az előző fordítottja
                        y++; //le 1-el
                    }
                    square[y][x] = n++;
                    Console.Clear();
                    foreach (int[] a in square) //bűvn kiíratása
                    {
                        foreach (int nm in a)
                            Console.Write("{0}\t", nm);
                        Console.WriteLine();
                    }
                    Console.WriteLine("X:{0} Y:{1}", x, y); //x-y kiíratása
                    if (v) Console.ReadKey(true); //lépés
                }
                Console.Clear();
                foreach (int[] a in square) //bűvn kiíratása
                {
                    foreach (int nm in a)
                        Console.Write("{0}\t", nm);
                    Console.WriteLine();
                }
                return square;
			}
            else if (size == 1)
            {
                int[][] empty = new int[1][];
                for (int i = 0; i < empty.Length; i++)
                {
                    empty[i] = new int[1];
                }
                return empty;
            }
            else
            {
                int sum = size * (size * size + 1) / 2; //a sorok/oszlopok/átlók összege
                
                int[][] empty = new int[size][]; //üres négyzet

                for (int i = 0; i < empty.Length; i++)
                {
                    empty[i] = new int[size];
                }

                if (recursive > 512) //feladjuk
                    return empty;

                int[][] square = (int[][])empty.Clone();
                square[1][1] = 5;

                ArrayList corners = new ArrayList() { new int[] { 0, 0 }, new int[] { 0, 2 }, new int[] { 2, 0 }, new int[] { 2, 2 } };
                ArrayList evens_original = new ArrayList() { 2, 4, 6, 8 };
                ArrayList evens = evens_original.Clone() as ArrayList;
                for (int i = 0; i < corners.Count * 2; i++)
                {
                    int next = r.Next(0, evens.Count);
                    int p = (int)evens[next];
                    evens.Remove(p);
                    int[] corner = corners[i % 4] as int[];
                    square[corner[1]][corner[0]] = p; //a sarokhoz hozzárakjuk a 
                    Console.WriteLine("Corner: {0},{1}, Value: {2} (Count: {3})", corner[0], corner[1], p, corners.Count);
                    if (evens.Count == 0)
                    {
                        evens = evens_original.Clone() as ArrayList;
                    }
                
                }
                List<int> nhsz = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9}; //nem használt számok, elméletileg 2 darab lesz
                for (int i = 0; i < square.Length; i++)
                {
                    if (i == 0 || i == 2)
                    {
                        square[i][1] = sum - square[i][0] - square[i][2];
                    }
                    for (int j = 0; j < square[i].Length; j++)
                    {
                        nhsz.Remove(square[i][j]);
                    }
                }
                foreach (int n in nhsz)
                {
                    Console.WriteLine("Megmaradt: {0}", n);
                }
                for (int i = 0; i < square[0].Length; i++)
                {
                    if (i == 0 || i == 2)
                    {
                        int n = nhsz[0];
                        square[1][i] = n;
                        nhsz.Remove(n);
                    }
                }
                if (CheckSquare(square))
                {
                    return square;
                }
                else
                {
                    return GenerateSquareNovus(size, v, recursive + 1); //recursive szar
                }
            }
		}

        /// <summary>
        /// Kivesz random db. négyzetet a bnégyzetből
        /// </summary>
        /// <param name="square">Bűvös négyzet</param>
        /// <returns>Hiányos bűvös négyzet</returns>
        public static int[][] Gamify(int[][] square)
        {
            int ntr = r.Next(1, (square.Length * square.Length));
            for (int i = 0; i < square.Length; i++)
            {
                if (ntr <= 0)
                {
                    break;
                }
                for (int j = 0; j < square[i].Length; j++)
                {
                    if (r.Next(0, 2) == 1)
                    {
                        square[i][j] = 0;
                        ntr--;
                    }
                }
            }
            return square;
        }

        /// <summary>
        /// Megvizsgálja egy int tömb tömb érvényes bűvösnégyzet-e
        /// </summary>
        /// <param name="square">Négyzet</param>
        /// <returns>A megadott négyzet bűvösnégyzet-e</returns>
        public static bool CheckSquare(int[][] square)
		{
            if (square.Length * square[0].Length == 1)
            {
                Console.WriteLine("1x1-es négyzet, minden érték jó");
                return true;
            }
			int rl = 0;
			int cl = 0;
			rl = square.Length;
			if (rl > 0)
				cl = square[0].Length;
			Console.WriteLine("Sorok: {0} Oszlopok: {1}", rl, cl);
			if (rl != cl) //ha a sorok és oszlopok száma nem egyenlő, akkor nem érvényes a négyzet
			{
				Console.WriteLine("Sorok és oszlopok száma nem egyezik");
				return false;
			}
			else
			{
				Console.WriteLine("Sorok és oszlopok száma egyezik");
			}
			///Utolsó sor összege
			int lastrowsum = ((rl * rl + 1) / 2) * rl;
			///OK
			bool ok = true;
			//Megvizsgál minden sort
			foreach (int[] row in square)
			{
				int sum = 0;
				foreach (int n in row)
				{
					sum += n;
				}
				Console.WriteLine("Utolsó sor összege: {0} Jelenlegi: {1}", lastrowsum, sum);
				if (sum != lastrowsum)
				{
					ok = false;
				}
				lastrowsum = sum;
			}
			Console.WriteLine("Sor ellenőrzés: {0}", (ok) ? "Sikeres" : "Sikertelen");
			//Megvizsgál minden oszlopot
			int lastcolumnsum = ((cl * cl + 1) / 2) * cl;
			for (int i = 0; i < square[0].Length; i++)
			{
				int sum = 0;
				for (int j = 0; j < square.Length; j++)
				{
					sum += square[j][i];
				}
				if (sum != lastcolumnsum)
				{
					ok = false;
				}
				lastcolumnsum = sum;
			}
			Console.WriteLine("Oszlop ellenőrzés: {0}", (ok) ? "Sikeres" : "Sikertelen");
			//átlós
			//balfelső-jobbalsó
			int x = 0, y = 0;
			int dsum = 0; //diagonal sum, az átlós ellenőrzések összegei ide kerülnek
			while (x < cl && y < rl)
			{
				dsum += square[y][x];
				x++;
				y++;
			}
			if (lastcolumnsum != dsum && lastrowsum != dsum) //feltételezzük hogy az oszlopok és sorok ellenőrzése sikeres volt (ha meg nem volt akkor már mindegy, nem?)
			{
				ok = false;
			}
			Console.WriteLine("BF-JA átlós ellenőrzés: {0}", (ok) ? "Sikeres" : "Sikertelen");
			//jobbfelső-balalsó
			x = cl - 1;
			y = rl - 1;
			dsum = 0;
			while (x >= 0 && y >= 0)
			{
				dsum += square[y][x];
				x--; //átlósan
				y--; //lépegetünk
			}
			if (lastcolumnsum != dsum && lastrowsum != dsum)
			{
				ok = false;
			}
			Console.WriteLine("JF-BA átlós ellenőrzés: {0}", (ok) ? "Sikeres" : "Sikertelen");

			Console.WriteLine((ok) ? "A négyzet bűvös." : "A négyzet nem bűvös.");
			return ok;
		}
	}
}

