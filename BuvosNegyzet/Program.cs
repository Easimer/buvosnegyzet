﻿#region LICENSE
/* The MIT License (MIT)
 * 
 * Copyright (c) 2015 Daniel Meszaros <easimer@easimer.tk>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#endregion

using System;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.IO;

namespace BuvosNegyzet
{
    class Program
    {
        private static Boolean quiet = true;

        /// <summary>
        /// Bűvösnégyzet gomb
        /// </summary>
        class BNButton : ButtonBase
        {
            public Color BorderColor = Color.Black;
            private Color ResetColor = SystemColors.ControlDark;
            public BNButton()
                : base()
            {
                this.BackColor = SystemColors.ControlDark;
                Font f = this.Font;
                this.Font = new Font("Courier", 22.0f);
            }
            protected override void OnPaint(PaintEventArgs pe)
            {
				if (ResetColor == Color.Black) {
                    ResetColor = GameForm.GetInstance().BackColor;
				}
				pe.Graphics.Clear(ResetColor);
                pe.Graphics.DrawString(this.Text, this.Font, new SolidBrush(Color.Black), new Point(0, 0));
                pe.Graphics.DrawRectangle(new Pen(BorderColor), new Rectangle(0, 0, this.Size.Width - 1, this.Size.Height - 1));
            }
        }
        /// <summary>
        /// A játék ablak
        /// </summary>
		class GameForm : Form
		{
            //Singleton
			private static GameForm instance = new GameForm ();

			public static GameForm GetInstance()
			{
				return instance;
			}

			public static GameForm NewInstance()
			{
				instance = new GameForm ();
				return instance;
			}

			public Panel GamePanel;
			public int[][] MagicSquare;
			public Tuple <int, int> Count;
			public BNButton[][] MSButtons;
			public NumericUpDown GameInputField;
			public MenuStrip TopMenu;
            public Timer GameTimer;
            public StatusStrip GameStatus;
            public ToolStripLabel GameTimerLabel;
            public long Time
            {
                get;
                private set;
            }
            private string TimeString;

            private BNButton current_real;
            public BNButton current
            {
                get
                {
                    return current_real;
                }
                set
                {
                    current_real = value;
                    if (LibMagicSquare.CheckSquare(BNButtonArrayToIntegerArray(MSButtons)))
                    {
                        GameForm.GetInstance().GameTimer.Stop();
                        long score = (MSButtons.Length + MSButtons[0].Length) * 10 - Time;
                        MessageBox.Show("Nyertél! Pontszám: " + score);
                    }
                    else
                    {
                        Console.WriteLine("Hmm...");
                    }
                }
            }
            /// <summary>
            /// GameForm konstruktor
            /// </summary>
			private GameForm()
			{
				this.Text = "Torg"; //beállítjuk a címét
				this.ShowIcon = false; //kikapcsoljuk az ikont
				this.Size = new System.Drawing.Size(640, 480); //beállítjuk a méretet
                this.DoubleBuffered = true; //duplapuffer
                GameTimer = new Timer();
                GameTimer.Interval = 1000;
                GameTimer.Tick += (object sender, EventArgs e) =>
                    {
                        Time++;
                        TimeSpan t = TimeSpan.FromSeconds(Time);
                        TimeString = string.Format("{0:D2}:{1:D2}:{2:D2}", t.Hours, t.Minutes, t.Seconds);
                        GameTimerLabel.Text = TimeString;
                    };

				GamePanel = new Panel();
				GamePanel.Dock = DockStyle.Fill;

				GameInputField = new NumericUpDown();
				GameInputField.Size = new Size(150, GameInputField.Size.Height);
				GameInputField.Location = new Point(24, 36);
				GameInputField.Visible = false; //alapból láthatatlan
				GameInputField.KeyPress += GameInput;
				GameInputField.BackColor = Color.Black;

                GameStatus = new StatusStrip();
                GameStatus.Dock = DockStyle.Bottom;
                GameTimerLabel = new ToolStripLabel();
                GameTimerLabel.Text = TimeString;
                GameStatus.Items.Add(GameTimerLabel);

				TopMenu = new MenuStrip ();
				TopMenu.Dock = DockStyle.Top;
                ToolStripMenuItem TopMenuGameDropDown = new ToolStripMenuItem("Játék");

                ToolStripMenuItem NewGameButton = new ToolStripMenuItem();
				NewGameButton.Text = "Új játék";
				NewGameButton.Click += (object sender, EventArgs e) => 
                    {
                        new NewGameForm().ShowDialog();
                    };
				TopMenuGameDropDown.DropDownItems.Add (NewGameButton);
                
                ToolStripMenuItem QuitButton = new ToolStripMenuItem();
				QuitButton.Text = "Kilépés";
				QuitButton.Click += (object sender, EventArgs e) => Application.Exit();
				TopMenuGameDropDown.DropDownItems.Add(QuitButton);

				TopMenu.Items.Add (TopMenuGameDropDown);

                ToolStripMenuItem TopMenuHelpDropDown = new ToolStripMenuItem("Súgó");
                TopMenuHelpDropDown.Click += (object sender, EventArgs e) =>
                    {
                        new HelperForm().ShowDialog();
                    };
                TopMenu.Items.Add(TopMenuHelpDropDown);
                this.KeyPreview = true; //childok blokkolják az inputot
				this.Controls.Add (TopMenu);
				this.Controls.Add(GamePanel);
				this.Controls.Add(GameInputField);
                this.Controls.Add(GameStatus);
				this.KeyPress += GameInput;
			}
            /// <summary>
            /// Új játék
            /// </summary>
            /// <param name="size">BN mérete</param>
			public void NewGame(int size, bool defective = true)
			{
				int n = size;
				if (n % 2 != 0 && n != 0)
				{
                    while (MagicSquare == null)
                    {
                        MagicSquare = LibMagicSquare.GenerateSquareNovus(n, false); //létrehozunk egy bűvös négyzetet
                    }
                    if(defective)
                    {
                        MagicSquare = LibMagicSquare.Gamify(MagicSquare); //hiányossá tétel
                    }
					GamePanel.Controls.Clear();
					MSButtons = GenerateButtons(MagicSquare, new Point(GameInputField.Location.X, GameInputField.Location.Y + GameInputField.Size.Height + 10));
					for (int i = 0; i < MSButtons.Length; i++)
					{
						for(int j = 0; j < MSButtons[i].Length; j++)
						{
							BNButton b = MSButtons[j][i];
							b.Name = "" + j + ',' + i;
							Console.WriteLine(b.Name);
							GamePanel.Controls.Add(b);

						}
					}
                    MagicSquare = null;
				}
				else
				{
					MessageBox.Show("Csak páratlan természetes szám írható be!", "Hiba");
				}
			}

			void GameInput(object sender, EventArgs e)
			{
                if ((e as KeyPressEventArgs).KeyChar == (char)Keys.Return)
                {
                    foreach (BNButton[] a in MSButtons)
                    {
                        foreach (BNButton bt in a)
                        {
                            Console.WriteLine(bt.Name);
                        }
                    }
                    current = null;
                    return;
                }
                else
                {
                    char c = (e as KeyPressEventArgs).KeyChar;
                    {
                        if (Char.IsDigit(c))
                        {
                            current.Text += c;
                            if (LibMagicSquare.CheckSquare(BNButtonArrayToIntegerArray(MSButtons)))
                            {
                                GameForm.GetInstance().GameTimer.Stop();
                                long score = (MSButtons.Length + MSButtons[0].Length) * 10 - Time;
                                MessageBox.Show("Nyertél! Pontszám: " + score);
                            }
                            else
                            {
                                Console.WriteLine("Hmm...");
                            }   
                        }
                    }

                }
			}


            /// <summary>
            /// BNButton kattintás event
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
			void BNButtonClick(object sender, EventArgs e)
			{
				current = sender as BNButton;
				GameInputField.Visible = true;
				Count = new Tuple<int, int>(Convert.ToInt32(current.Name.Split(',')[0]), Convert.ToInt32(current.Name.Split(',')[1]));
				current.Text = "";
				foreach (BNButton[] a in MSButtons)
				{
					foreach (BNButton bt in a)
					{
						bt.BorderColor = Color.Black;
						bt.Refresh(); //kirajzolás erőltetése
					}
				}
				current.BorderColor = Color.Gold;
			}

			/// <summary>
			/// Bűvösnégyzetből gombtömb generálása
			/// </summary>
			/// <param name="ms">Bűvösnégyzet</param>
			/// <param name="o">Origó</param>
			/// <returns>Bűvösnégyzet gombokból</returns>
			BNButton[][] GenerateButtons(int[][] ms, Point o)
			{
				Random r = new Random();
				int c = 0;
				int w = ms[0].Length;
				BNButton[][] btna = new BNButton[w][];
				for(int i = 0; i < ms.Length; i++)
				{
					btna[i] = new BNButton[ms[i].Length];
					for(int j = 0; j < ms[i].Length; j++)
					{
						BNButton b = new BNButton();
						if (c <= 3 && r.NextDouble() < 0.5)
						{
							c++;
						}
						else
						{
                            string t = ms[i][j].ToString();
                            if (t.Equals("0"))
                            {
                                t = "";
                            }
							b.Text = t;
						}
						b.Text = ms[i][j].ToString();
						b.Size = new Size(50, 50);
						b.Location = new Point(o.X + (j * 50) - 1, o.Y + (i * 50) - 1);
						b.Click += BNButtonClick;
						btna[i][j] = b;
					}
				}
				return btna;
			}

			class NewGameForm : Form
			{
				private NumericUpDown InputField;
				private Button OkButton;
                private CheckBox DefectiveCheckBox;
                private Label sizel;
                private static decimal LastSize = 1; //utolsó méret
				public NewGameForm()
				{
                    Console.WriteLine("Utolsó méret: {0}", LastSize);
                    GameForm.GetInstance().GameTimer.Stop();
					this.FormBorderStyle = FormBorderStyle.FixedDialog;
					this.Size = new Size(300, 100);
					this.Text = "Új játék";
					InputField = new NumericUpDown();
                    InputField.Value = LastSize;
					InputField.Maximum = decimal.MaxValue;
					InputField.Location = new Point(24, 30);
                    InputField.KeyPress += (object sender, KeyPressEventArgs e) =>
                        {
                            if (e.KeyChar == (char)Keys.Return)
                            {
                                GameForm.GetInstance().NewGame(Convert.ToInt32(InputField.Value));
                                GameForm.GetInstance().GameTimer.Start();
                                LastSize = InputField.Value;
                                this.Close();
                            }
                        };
					this.Controls.Add(InputField);
                    OkButton = new Button();
                    OkButton.Location = new Point(InputField.Location.X + InputField.Size.Width + 16, InputField.Location.Y);
                    OkButton.Size = new Size(OkButton.Size.Width, InputField.Size.Height);
                    OkButton.Text = "Ok";
                    OkButton.Click += (object sender, EventArgs e) =>
                        {
                            GameForm.GetInstance().NewGame(Convert.ToInt32(InputField.Value), DefectiveCheckBox.Checked);
                            GameForm.GetInstance().GameTimer.Start();
                            LastSize = InputField.Value;
                            this.Close();
                        };
                    this.Controls.Add(OkButton);
                    sizel = new Label();
                    sizel.Text = "Pálya mérete:";
                    sizel.Location = new Point(10, 10);
                    this.Controls.Add(sizel);
                    DefectiveCheckBox = new CheckBox();
                    DefectiveCheckBox.Checked = true;
                    DefectiveCheckBox.Location = new Point(InputField.Location.X, InputField.Location.Y + InputField.Size.Height);
                    DefectiveCheckBox.Text = "Hiányos";
                    this.Controls.Add(DefectiveCheckBox);
				}
			}

            class HelperForm : Form
            {
                private TextBox tb;
                public HelperForm()
                {
                    this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
                    this.Text = "Súgó";
                    this.Size = new System.Drawing.Size(350, 500);
                    this.ShowIcon = false;
                    tb = new TextBox();
                    //tb.Location = new Point(10, 10);
                    tb.Multiline = true;
                    tb.Dock = DockStyle.Fill;
                    //tb.Size = new System.Drawing.Size(330, 460);
                    tb.ReadOnly = true;
                    tb.Text = @"A bűvös négyzet érdekes, sok szabályszerűséget mutató számelrendezés. Egy négyzetet az oldalaival párhuzamos egyenesekkel bizonyos számú sorra és ugyanannyi oszlopra osztunk fel, majd a következő feladatot tűzzük ki: töltsük meg ezt az ábrát egymás utáni természetes számokkal úgy, hogy minden kis mezőbe egy szám jusson, és a számok összege minden sorban, minden oszlopban és mindkét átló mentén ugyanannyi legyen.
Új játék a Játék menüpont Új játék gombjára való kattintással indítható. A megnyíló ablakban megadható a négyzet mérete és hogy hiányos legyen-e. A pálya mérete csak páratlan lehet (kivéve 1x1).
Mezőbe íráshoz rá kell kattintani arra, lenyomni a számbillentyűket tetszőleges sorrendben és entert ütni. Ezzel a szám bekerült a mezőbe.
Minden egyes ilyen írás után megnézi a játék hogy helyes-e a kitöltött négyzet.";
                    this.Controls.Add(tb);
                }
            }
		}
        
        /// <summary>
        /// Entry point
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            foreach (string s in args)
                switch (s)
                {
                    case "-v":
                        quiet = false;
                        break;
                    default:
                        Console.WriteLine("Ismeretlen paraméter: {0}", s);
                        break;
                }
            if (quiet)
            {
                Console.SetOut(new StreamWriter(Stream.Null, Encoding.Default));
            }
			GameForm gameForm = GameForm.GetInstance ();
			gameForm.ShowDialog ();
            gameForm.Activate();
            gameForm.Focus();
        }

        /// <summary>
        /// Átalakít egy BN gomb tömböt integer tömbbé
        /// </summary>
        /// <param name="a">BN gomb tömb</param>
        /// <returns>Integer tömb</returns>
        static int[][] BNButtonArrayToIntegerArray(BNButton[][] a)
        {
            int[][] a2 = new int[a.Length][];
            for (int y = 0; y < a.Length; y++)
            {
                a2[y] = new int[a[y].Length];
                for (int x = 0; x < a[y].Length; x++)
                {
                    if (a[y][x].Text != "")
                        a2[y][x] = Convert.ToInt32(a[y][x].Text);
                    else
                        a2[y][x] = 0;
                }
            }
            return a2;
        }


    }
}
